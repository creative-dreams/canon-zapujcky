import Vuex from 'vuex'

const createStore = () => {
	return new Vuex.Store({
		state: {
			auth: false,
		},
		mutations: {
			/* setLoadedDataPlaces (state, payload) {
				state.loadedDataPlaces = payload
			} */
		},
		actions: {
			/* loadDataPlaces({ commit }) {
				this.$axios.get('https://jsonplaceholder.typicode.com/photos').then((response) => {
					commit('setLoadedDataPlaces', response.data)
				})
			} */
		},
		getters: {
			/* eventType(state) {
				return state.eventType
			} */
			auth(state) {
				return state.auth
			}
		}
	})
}

export default createStore